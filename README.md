# Learning Programming in Go

This is a work-in-progress collection of assorted tutorials and other materials intended for people who want to learn how to program in Go. 

# Why Learn Go?

Go is a very cleanly designed programming language that offers its users a set of simple and reliable tools that can be used together to write all kinds of common programs. The concepts are not new, and are shared among most common programming languages. That quality makes any knowledge of programming in Go very easy to transfer into other popular languages. The design of Go encourages a focus on testing, documentation, stable interfaces, and modular reusable components, which are arguably more important to any kind of programming than any particular language or tool. 

Go currently has a reputation of being well-suited to people who already know how to program in a different language, but I think that's probably only because there isn't much educational material focused on learning Go as a first programming language. 

# Contents

